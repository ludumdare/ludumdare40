﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Data;
using Game.Helpers;

namespace Game.Managers
{
    public enum GameStates
    {
        GAME_HOME,
        GAME_ABOUT,
        GAME_INFO,
        GAME_SETTINGS,
        GAME_OVER,
        GAME_WIN,
        GAME_RUNNING
    }

    public class GameManager : MonoSingleton<GameManager>
    {
        public GameData Game { get; set; }
        public SettingsData Settings { get; set; }
        public GameStates GameState { get; set; }
        public PlayerData Player { get; set; }
        public SessionData Session { get; set; }
        public LevelData Level { get; set; }

        public void StartNewGame()
        {
            D.Trace("[GameManager] StartNewGame");
            Session = new SessionData();
            Session.Score = 0;
            Session.Level = 1;
            Session.Power = 250;
            Session.Lives = 1;
            Session.PowerNeeded = 500;
            GameState = GameStates.GAME_RUNNING;
        }

        public void ContinueGame()
        {
            D.Trace("[GameManager] ContinueGame");
        }

        //  load player saved slot
        public PlayerData LoadPlayer(int position)
        {
            D.Trace("[GameManager] LoadPlayer");
            PlayerData playerData = new PlayerData();
            return playerData;
        }

        //  save player data to specified slot
        public void SavePlayer(int position)
        {
            D.Trace("[GameManager] SavePlayer");
        }

        //  load game settings (will override defaults)
        public SettingsData LoadGameSettings(SettingsData settings)
        {
            D.Trace("[GameManager] LoadGameSettings");
            //settings.ControlLeft = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("ControlLeft"));
            //settings.ScreenSize = 1;
            return settings;
        }

        //  save game settings
        public void SaveGameSettings()
        {
            D.Trace("[GameManager] SaveSettings");
        }

        private void OnEnable()
        {
            D.Trace("[GameManager] OnEnable");
            GameState = GameStates.GAME_HOME; 
        }

    }
}