﻿using UnityEngine;
using System.Collections.Generic;

namespace Game.Managers
{
    public class MusicManager : MonoSingleton<MusicManager>
    {
        public AudioClip[] Clips;

        private AudioSource _audioSource;
        private Dictionary<string, AudioClip> _clips;
        private float _volume = 1.0f;

        public void PlayMusic(string name)
        {
            D.Trace("[MusicManager] PlayMusic ( name:{0} )", name);
            playMusic(_clips[name]);
        }

        public void PlayMusic(string name, float volume)
        {
            D.Trace("[MusicManager] PlayMusic ( name:{0}, volume:{1} )", name, volume);
            playMusic(_clips[name], 1.0f, volume);
        }

        public void SetVolume(float volume)
        {
            D.Trace("[MusicManager] SetVolume( volume:{0} )", volume);
            _volume = volume;
        }

        //	PRIVATE

        private void playMusic(AudioClip clip)
        {
            D.Trace("[MusicManager] playMusic( clip:{0} )", clip.name);
            playMusic(clip, 1.0f, _volume);
        }

        private void playMusic(AudioClip clip, float pitch, float volume)
        {
            D.Trace("[MusicManager] playMusic( clip:{0}, pitch:{1}, volume:{2} )", clip.name, pitch, volume);
            _audioSource.clip = clip;
            _audioSource.volume = volume;
            _audioSource.pitch = pitch;
            _audioSource.loop = true;
            _audioSource.Play();
            name = string.Format("Music ({0})", clip.name);
        }

        //  MONO

        private void Start()
        {
            D.Trace("[MusicManager] Start");
            _clips = new Dictionary<string, AudioClip>();
            foreach (AudioClip clip in Clips)
            {
                _clips.Add(clip.name, clip);
            }
        }

        private void OnEnable()
        {
            D.Trace("[MusicManager] OnEnable");
            _audioSource = gameObject.AddComponent<AudioSource>();
            DontDestroyOnLoad(gameObject);
        }

    }
}