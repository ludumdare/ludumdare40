﻿using Game.Data;
using Game.Factories;
using Game.Managers;
using Game.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Game.Controllers
{
    public class GameController : Controller
    {
        public LevelData Level;
        public SpriteRenderer spriteFader;
        public Text gameMessageText;
        public Text debugText;

        public PlayerController PlayerController;
        public CloudController CloudController;
        public DebrisController DebrisController;
        public PrefabFactory PrefabFactory;
        public InputController InputController;
        public PlaneController PlaneController;

        public PowerBarElement powerBar;
        public EnemyBarElement enemyBar;
        public ScoreElement scoreElement;

        private StageData stageData;
        private List<SpawnController> spawnControllers;
        private List<SpawnerData> spawnerData;

        private GameManager gameManager;
        private WaveData waveData;

        public int waveCycle;
        public int nextWaveCycle;
        private bool waveActive;

        public void StartGame()
        {
            CloudController.InitController();
            DebrisController.InitController();
            PlayerController.InitController();
            InputController.InitController();
            PlaneController.InitController();

            CloudController.StartController();
            DebrisController.StartController();
            PlayerController.StartController();
            InputController.StartController();
            PlaneController.StartController();

            powerBar.InitElement();
            enemyBar.InitElement();
            scoreElement.InitElement();

            powerBar.StartElement();
            enemyBar.StartElement();
            scoreElement.StartElement();

            spawnControllers = new List<SpawnController>();

            gameManager.Session.Level = 0;
            gameManager.Session.Stage = 0;
            gameManager.Session.Wave = 0;  

            //gameManager.Level = Level;
            //gameManager.Level.EnemyPower = Level.EnemyStartingPower;

            stageData = gameManager.Level.LevelStages[gameManager.Session.Stage];

            gameManager.Level.EnemyPower = gameManager.Level.EnemyStartingPower;

            NextWave();

        }

        public void StopGame()
        {
            PlayerController.StopController();
            CloudController.StopController();
            DebrisController.StopController();
            InputController.StopController();
            PlaneController.StopController();

            powerBar.StopElement();
            enemyBar.StopElement();
            scoreElement.StopElement();

        }

        public void StartLevel()
        {

        }

        public void WaveOver()
        {
            waveActive = false;

            foreach (SpawnController spawnController in spawnControllers)
            {
                spawnController.StopController();
                Destroy(spawnController.gameObject);
            }

            spawnControllers = new List<SpawnController>();

            gameManager.Session.Wave += 1;

            gameMessageText.text = "wave completed";

            Sequence seq = DOTween.Sequence();
            seq.Append(gameMessageText.DOFade(0.0f, 0.0f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(0.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(0.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(0.0f, 1.50f));
            seq.AppendCallback(NextWave);
            seq.Play();

        }

        public void NextWave()
        {
            gameMessageText.text = "next wave";

            Sequence seq = DOTween.Sequence();
            seq.Append(gameMessageText.DOFade(0.0f, 0.0f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(0.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(0.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(0.0f, 1.50f));
            seq.Play();

            waveData = stageData.StageWaves[gameManager.Session.Wave];
            spawnerData = waveData.Spawners;

            foreach (SpawnerData spawnData in spawnerData)
            {
                D.Log(spawnData.Spawner.name);
                SpawnController sc = Instantiate(spawnData.Spawner);
                sc.InitController();
                spawnControllers.Add(sc);
            }

            foreach (SpawnController spawnController in spawnControllers)
            {
                spawnController.StartController();
            }

            waveCycle = 0;
            nextWaveCycle = waveData.WaveCycles;
            waveActive = true;

        }

        private void goHome()
        {
            SceneManager.LoadScene(gameManager.Game.HomeScene);
        }

        public void AddScore(int points)
        {
            gameManager.Session.Score += points;
        }

        public void AddLife()
        {
            gameManager.Session.Lives += 1;
        }

        public void AddPower(int power)
        {
            gameManager.Session.Power += power;

            if (gameManager.Session.Power < 0)
                RemoveLife();

            if (gameManager.Session.Power > gameManager.Session.PowerNeeded)
                gameManager.Session.Power = gameManager.Session.PowerNeeded;
        }

        public void RemoveLife()
        {
            gameManager.Session.Lives -= 1;

            if (gameManager.Session.Lives <= 0)
            {
                gameOver();
            }
        }

        public void WinGame()
        {
            StopGame();

            gameMessageText.text = "game win";

            Sequence seq = DOTween.Sequence();
            seq.Append(gameMessageText.DOFade(0.0f, 0.0f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(0.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(0.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(0.0f, 1.50f));
            seq.AppendCallback(goHome);
            seq.Play();
        }

        public void PauseGame()
        {
            Pauseable[] pauseable = GameObject.FindObjectsOfType<Pauseable>();
            for (int i = 0; i < pauseable.Length; i++)
            {
                pauseable[i].Pause();
            }
        }

        public void ResumeGame()
        {
            Pauseable[] pauseable = GameObject.FindObjectsOfType<Pauseable>();
            for (int i = 0; i < pauseable.Length - 1; i++) 
            {
                pauseable[i].Resume();
            }
        }

        private void gameOver()
        {
            StopGame();

            gameMessageText.text = "game over";

            Sequence seq = DOTween.Sequence();
            seq.Append(gameMessageText.DOFade(0.0f, 0.0f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(0.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(0.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(1.0f, 0.15f));
            seq.Append(gameMessageText.DOFade(0.0f, 4.50f));
            seq.AppendCallback(goHome);
            seq.Play();
        }

        private void Update()
        {

            if (waveActive)
            {
                waveCycle += 1;

                if (waveCycle > nextWaveCycle)
                {
                    WaveOver();
                }
            }

            debugText.text = string.Format("waveActive: {0}\nwaveCycle: {1}\nnextWave: {2}", waveActive, waveCycle, nextWaveCycle);

            if (GameManager.Instance.Settings.CheatsOn)
            {
                string key = Input.inputString;
                D.Fine("- key:{0}", key);

                if (key == "0")
                {
                    D.Trace("[CheatCode] Weapon 0 - Kick");
                    PlayerController.ChangeWeapon(null);
                }

                if (key == "1")
                {
                    D.Trace("[CheatCode] Weapon 1 - Knife");
                    PlayerController.ChangeWeapon("knife");
                }

                if (key == "2")
                {
                    D.Trace("[CheatCode] Weapon 2 - Pistol");
                    PlayerController.ChangeWeapon("pistol");
                }

                if (key == "3")
                {
                    D.Trace("[CheatCode] Weapon 3 - Rifle");
                    PlayerController.ChangeWeapon("rifle");
                }

                if (key == "4")
                {
                    D.Trace("[CheatCode] Weapon 4 - Star");
                    PlayerController.ChangeWeapon("star");
                }

                if (key == "g")
                {
                    D.Trace("[CheatCode] God Mode");
                }

                if (key == "r")
                {
                    D.Trace("[CheatCode] Restart");
                }

                if (key == "p")
                {
                    D.Trace("[CheatCode] Pause");
                    if (!IsPaused)
                    {
                        PauseGame();
                    } else
                    {
                        ResumeGame();
                    }
                }

                if (key == "z")
                {
                    D.Trace("[CheatCode] SuperWeapon");
                    AddPower(999);
                }
            }
        }

        public override void OnEnable()
        {
            base.OnEnable();
            gameManager = GameManager.Instance;
        }
    }
}