﻿using Sdn.SpriteBoss;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Controllers
{
    /*
    *   Controls Other Spawners
    */
    public class SpawnController : Controller
    {
        public Spawner[] spawners;

        public int CycleMin;
        public int CycleMax;

        public float MinSpeed;
        public float MaxSpeed;

        public int Total=1;

        public bool RunOnStart;

        private int cycle;
        private int nextCycle;

        protected Transform root;

        public override void StopController()
        {
            base.StopController();
            Destroy(root.gameObject, 100);
        }

        private void Update()
        {
            if (!IsActive)
                return;

            if (IsPaused)
                return;

            cycle += 1;

            if (cycle >= nextCycle)
            {
                nextCycle = Random.Range(CycleMin, CycleMax);
                cycle = 0;
                for (int z = 0; z < Total; z++)
                {
                    int i = Random.Range(0, spawners.Length);
                    Spawner s = Instantiate(spawners[i].gameObject).GetComponent<Spawner>();
                    NextCycle(s);
                }
            }
        }

        public virtual void NextCycle(Spawner spawn)
        {
            spawn.name = "spawner-" + System.Guid.NewGuid();
            spawn.Speed = Random.Range(MinSpeed, MaxSpeed);
            spawn.transform.parent = root;
            spawn.transform.position = new Vector2(500, Random.Range(-130, 130));
        }

        public override void OnEnable()
        {
            base.OnEnable();
            root = new GameObject("__spawners__").transform;
            if (RunOnStart)
                isActive = true;
        }
    }
}