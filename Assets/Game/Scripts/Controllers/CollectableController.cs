﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Controllers
{
    public class CollectableController : SpawnController
    {
        private Transform hotZone;

        public override void NextCycle(Spawner spawn)
        {
            base.NextCycle(spawn);
            spawn.transform.position = hotZone.position;
        }

        public override void OnEnable()
        {
            base.OnEnable();
            Destroy(root.gameObject);
            root = new GameObject("__collectables__").transform;
            hotZone = GameObject.Find("PlaneHotZone").transform;
        }
    }
}