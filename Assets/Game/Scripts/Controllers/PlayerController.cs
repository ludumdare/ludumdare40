﻿using Game.Managers;
using Sdn.SpriteBoss;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Controllers
{
    public class PlayerController : Controller
    {
        public int KickLength;

        private Player player;
        private InputController input;
        private WeaponController weaponController;
        private SpriteBoss spriteBoss;
        private string weaponName;
        private Weapon weaponTemplate;
        private GameObject kick;

        public int superCharge;

        //  generic weapon cycle info
        public int weaponActiveCycle;
        public int nextWeaponActiveCycle;
        public int weaponReadyCycle;
        public int nextWeaponReadyCycle;
        public bool weaponReady;
        public bool weaponActive;

        //  kick cycle info
        public int kickCycle;
        public int nextKickCycle;
        public bool kickReady;
        public bool kickActive;

        //  super weapon info
        public int superActiveCycle;
        public int nextSuperActiveCycle;
        public int superReadyCycle;
        public int nextSuperReadyCycle;
        public bool superReady;
        public bool superActive;


        public void ChangeWeapon(string name)
        {
            D.Trace("[PlayerController] ChangeWeapon");

            if (weaponTemplate != null)
                Destroy(weaponTemplate.gameObject);

            this.weaponName = name;

            if (name == "kick")
            {
                weaponActive = false;
                weaponReady = false;
                kickActive = true;
                kickReady = true;
                return;
            }

            kickActive = false;
            kickReady = false;

            //  change weapon
            weaponTemplate = weaponController.CreateWeapon(weaponName);
            weaponTemplate.transform.position = new Vector2(-10000, -10000);
            weaponActiveCycle = 0;
            nextWeaponActiveCycle = weaponTemplate.ActiveTime;
            weaponReady = true;
            weaponActive = true;

            input.AutoFire = weaponTemplate.Auto;
            spriteBoss.SetSprite(name);
        }

        public int SuperCharge
        {
            get { return superCharge; }
        }

        public void AddSuperCharge(int charge)
        {
            D.Trace("[PlayerController] AddSuperCharge");
            superCharge += charge;
            if (superCharge > 100)
                superCharge = 100;
        }

        public void Kick()
        {
            D.Trace("[PlayerController] Kick");

            //  change sprite to kick
            spriteBoss.SetSprite("kick");

            //  activate kick collider
            kick.SetActive(true);

            //  reset cycle counter
            kickCycle = 0;
            nextKickCycle = KickLength;
            kickReady = false;
        }

        public void SuperWeapon()
        {
            D.Trace("[PlayerController] SuperWeapon");

            kickActive = false;
            kickActive = false;
            weaponActive = false;
            weaponReady = false;

            //  change sprite to super weapon pose

            //  create weapon
            weaponTemplate = weaponController.CreateWeapon("super");
            superActiveCycle = 0;
            nextSuperActiveCycle = weaponTemplate.ActiveTime;
            superReadyCycle = 0;
            nextSuperReadyCycle = weaponTemplate.DelayTime;
            superReady = true;
            superActive = true;
            spriteBoss.SetSprite("super");
        }

        private void Update()
        {
            if (!IsActive)
                return;

            if (IsPaused)
                return;

            // next move - kicking prevents movement

            transform.Translate(Vector2.right * input.HorizontalAxis * player.Speed * Time.deltaTime);
            transform.Translate(Vector2.up * input.VerticalAxis * player.Speed * Time.deltaTime);

            //  gravity - always happens

            transform.Translate(Vector2.down * player.Gravity * Time.deltaTime);

            //  make sure not going over boundries

            float playerX = transform.position.x;
            float playerY = transform.position.y;

            if (playerX > 120)
                playerX = 120;

            if (playerX < -220)
                playerX = -220;

            if (playerY > 115)
                playerY = 115;

            if (playerY < -115)
                playerY = -115;

            transform.position = new Vector2(playerX, playerY);

            //  check for weapon activation

            if (input.Fire1)
            {
                if (kickActive && kickReady)
                {
                        Kick();
                }

                if (weaponActive && weaponReady)
                {
                    Weapon w = Instantiate(weaponTemplate, weaponTemplate.transform.parent);
                    w.transform.position = transform.position;
                    w.FireWeapon();
                    weaponReadyCycle = 0;
                    nextWeaponReadyCycle = weaponTemplate.DelayTime;
                    weaponReady = false;
                }
            }

            if (GameManager.Instance.Session.Power >= GameManager.Instance.Session.PowerNeeded)
            {
                if (input.Fire2)
                {
                    SuperWeapon();
                }
            }

            //  handle kicking

            if (kickActive && !kickReady)
            {

                kickCycle += 1;

                //  check the kick cycle

                if (kickCycle > nextKickCycle)
                {
                    //  remove kick collider
                    kick.SetActive(false);

                    //  reset sprite
                    spriteBoss.SetSprite("idle");
                    kickReady = true;
                }
            }

            //  handle weapons

            if (weaponActive)
            {
                if (!weaponReady)
                    weaponReadyCycle += 1;

                //  check the weapon cycle

                if (weaponReadyCycle > nextWeaponReadyCycle)
                {
                    //  reset sprite

                    //  reset sprite

                    weaponReady = true;
                }

                //  active weapon countdown

                weaponActiveCycle += 1;

                if (weaponActiveCycle > nextWeaponActiveCycle)
                {
                    ChangeWeapon("kick");
                }
            }

            if (superActive)
            {
                superReadyCycle += 1;

                if (superReadyCycle > nextSuperReadyCycle)
                {
                    //  fire super again
                    GameManager.Instance.Session.Power -= 10;
                    superReadyCycle = 0;
                    for (int i = 0; i < 5; i++)
                    {
                        Weapon w = Instantiate(weaponTemplate, weaponTemplate.transform.parent);
                        w.transform.position = transform.position;
                        Vector3 v = new Vector2(0, Random.Range(-10, 10));
                        w.transform.position += v;
                        w.FireWeapon();
                    }
                }
                
                if (GameManager.Instance.Session.Power <= 0 )
                {
                    superActive = false;
                    superReady = false;
                    superReadyCycle = 0;
                    nextSuperReadyCycle = 0;
                    ChangeWeapon("kick");
                }
            }

        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            D.Trace("[PlayerController] OnTriggerEnter2D");
        }

        public override void OnEnable()
        {
            base.OnEnable();
            D.Trace("[PlayerController] OnEnable");
            player = GetComponent<Player>();
            input = GameObject.FindObjectOfType<InputController>();
            weaponController = GameObject.FindObjectOfType<WeaponController>();
            spriteBoss = GetComponent<SpriteBoss>();
            kick = transform.Find("Kick").gameObject;
            kick.SetActive(false);
            kickActive = true;
            kickReady = true;
            superActive = false;
            weaponActive = false;
        }
    }
}