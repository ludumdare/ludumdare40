﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Controllers
{
    /*
    *   Input can be either Joystick or Keyboard
    *   please make sure that the player > input settings are
    *   set as follows:
    *   
    *   Keyboard:
    *   
    *       Horizontal; Gravity=9; Dead=0.001; Sensitivity=9; Snap=True
    *       Vertical; Gravity=9; Dead=0.001; Sensitivity=9; Snap=True
    *   
    *   Joystick:
    *   
    *       Horizontal; Gravity=0; Dead=0.019; Sensitivity=1; Snap=False
    *       Vertical; Gravity=0; Dead=0.019; Sensitivity=1; Snap=False
    */
    public class InputController : Controller
    {
        private float verticalAxis;
        private float horizontalAxis;
        private bool fire1;
        private bool fire2;
        private string lastKey;
        private bool auto;

        public bool AutoFire
        {
            set { auto = value; }
        }
        public float VerticalAxis
        {
            get { return verticalAxis; }
        }

        public float HorizontalAxis
        {
            get { return horizontalAxis; }
        }

        public bool Fire1
        {
            get { return fire1; }
        }

        public bool Fire2
        {
            get { return fire2; }
        }

        public string LastKey
        {
            get { return lastKey; }
        }

        private void Update()
        {
            if (!IsActive)
                return;

            if (IsPaused)
                return;

            verticalAxis = Input.GetAxis("Vertical");
            horizontalAxis = Input.GetAxis("Horizontal");

            if (auto)
            {
                fire1 = Input.GetButton("Fire1");
            }
            else
            {
                fire1 = Input.GetButtonDown("Fire1");
            }

            fire2 = Input.GetButtonDown("Fire2");

            lastKey = Input.inputString;
        }

        public override void OnEnable()
        {
            base.OnEnable();
        }
    }
}