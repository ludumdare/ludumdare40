﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Game.Controllers
{
    public class PlaneController : Controller
    {
        public Vector2 planeMovementRange;

        private int cycle;
        private int nextCycle;

        private Plane plane;

        private int state;
        private Vector2 startPosition;

        private void Update()
        {
            if (!IsActive)
                return;

            if (IsPaused)
                return;

            float newX = 0;
            float newY = 0;
            bool tween = false;

            //  move the plane to random places every
            //  few cycles

            cycle += 1;

            float shakeX = Random.Range(-2, 2);
            float shakeY = Random.Range(-2, 2);

            newX = transform.position.x + shakeX;
            newY = transform.position.y + shakeY;

            if (cycle > nextCycle)
            {

                nextCycle = getNextCycle();
                cycle = 0;

                float nextX = Random.Range(-10, 10);
                float nexty = Random.Range(-10, 10);

                newX = transform.position.x + nextX;
                newY = transform.position.y + nexty;

                tween = true;
            }


            if (newX <= startPosition.x - planeMovementRange.x)
                newX = startPosition.x - planeMovementRange.x;

            if (newX >= startPosition.x + planeMovementRange.x)
                newX = startPosition.x + planeMovementRange.x;

            if (newY <= startPosition.y - planeMovementRange.y)
                newY = startPosition.y - planeMovementRange.y;

            if (newY >= startPosition.y + planeMovementRange.y)
                newY = startPosition.y + planeMovementRange.y;

            if (tween)
            {
                transform.DOLocalMoveY(newY, 0.5f);
                transform.DOLocalMoveX(newX, 0.5f);
                tween = false;
            } else
            {
                transform.position = new Vector3(newX, newY);
            }
        }

        public override void OnEnable()
        {
            base.OnEnable();
            D.Trace("[PlaneController] OnEnable");

            plane = GetComponent<Plane>();
            cycle = 0;
            nextCycle = getNextCycle();
            startPosition = transform.position;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireCube(startPosition, planeMovementRange);
        }

        private int getNextCycle()
        {
            D.Fine("[PlaneController] getNextCycle");
            return Random.Range(25, 50);
        }

        public override void InitController()
        {
            base.InitController();
            plane.BaseDamage = gameController.Level.EnemyStartingPower;
        }
    }
}