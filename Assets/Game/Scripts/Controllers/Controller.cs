﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Controllers
{
    public class Controller : Pauseable
    {
        protected bool isActive;

        public bool IsActive
        {
            get { return isActive; }
        }

        public virtual void InitController()
        {
            D.Trace("[Controller] InitController");
        }
        public virtual void StartController()
        {
            D.Trace("[Controller] StartController");
            isActive = true;
        }

        public virtual void StopController()
        {
            D.Trace("[Controller] StopController");
            isActive = false;
        }

        public override void OnEnable()
        {
            base.OnEnable();
            D.Trace("[Controller] OnEnable");
        }

    }
}