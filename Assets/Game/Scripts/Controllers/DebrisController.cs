﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Controllers
{
    public class DebrisController : SpawnController
    {
        public override void OnEnable()
        {
            base.OnEnable();
            Destroy(root.gameObject);
            root = new GameObject("__debris__").transform;
        }
    }
}