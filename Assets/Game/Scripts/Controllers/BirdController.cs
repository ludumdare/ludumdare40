﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Controllers
{
    public class BirdController : SpawnController
    {
        public override void OnEnable()
        {
            base.OnEnable();
            Destroy(root.gameObject);
            root = new GameObject("__birds__").transform;
        }
    }
}