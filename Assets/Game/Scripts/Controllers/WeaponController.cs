﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Game.Controllers
{
    public class WeaponController : MonoBehaviour
    {
        public Weapon WeaponKnife;
        public Weapon WeaponPistol;
        public Weapon WeaponRifle;
        public Weapon WeaponStar;
        public Weapon WeaponSuper;

        private GameObject root;

        public Weapon CreateWeapon(string weaponName)
        {
            if (weaponName == "knife")
            {
                Weapon go = Instantiate(WeaponKnife) as Weapon;
                go.transform.parent = root.transform;
                return go;
            }

            if (weaponName == "pistol")
            {
                Weapon go = Instantiate(WeaponPistol) as Weapon;
                go.transform.parent = root.transform;
                return go;
            }

            if (weaponName == "rifle")
            {
                Weapon go = Instantiate(WeaponRifle) as Weapon;
                go.transform.parent = root.transform;
                return go;
            }

            if (weaponName == "star")
            {
                Weapon go = Instantiate(WeaponStar) as Weapon;
                go.transform.parent = root.transform;
                return go;
            }

            if (weaponName == "super")
            {
                Weapon go = Instantiate(WeaponSuper) as Weapon;
                go.transform.parent = root.transform;
                return go;
            }

            return null;
        }

        private void OnEnable()
        {
            root = new GameObject("__weapons__");
        }
    }
}