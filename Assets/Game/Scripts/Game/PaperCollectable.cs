﻿using Game.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    /*
     *  An object that will be collected in the
     *  Game by the Player for Points or
     *  Advancement.
     *  
     */
    public class PaperCollectable : Spawner
    {
        public int Value = 10;   //  added to score
        public float Gravity = 2;

        public int stage;
        private int lastStage;
        private int nextStage = -1;

        public int cycle;
        private int nextCycle;

        private float nextX;
        private float nextY;
        private Vector2 nextVector = Vector2.zero;

        public override void OnEnable()
        {
            base.OnEnable();
            lastStage = 9999;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Player")
            {
                gameController.AddScore(Value);
                string effectValue = "EffectScore" + Value.ToString();
                gameController.PrefabFactory.CreatePrefab(effectValue);
                Destroy(gameObject);
            }
        }

        public virtual void CollectMe()
        {
            D.Trace("[Collectible] CollectMe");
            gameController.AddScore(Value);
        }

        public override void UpdateSpawner()
        {
            if (IsPaused)
                return;

            if (stage > lastStage)
                return;

            cycle += 1;

            if (cycle > nextCycle)
            {
                cycle += 1;
                stage += 1;
                nextCycle = 100;
            }

            if (stage > nextStage)
            {
                nextStage = stage;
                nextX = Random.Range(-1, 2);
                nextY = Random.Range(-1, 2);
                nextVector = new Vector2(nextX, nextY);
            }

            transform.Translate(Vector2.left * (Speed/2) * Time.deltaTime);
            transform.Translate(nextVector * (Speed) * Time.deltaTime);

            //  make sure not going over boundries

            float playerX = transform.position.x;
            float playerY = transform.position.y;

            if (playerY > 155)
                playerY = 155;

            if (playerY < -155)
                playerY = -155;

            transform.position = new Vector2(playerX, playerY);
        }
    }
}