﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Game {
    public class Obstacle : Spawner
    {
        public virtual void DestroyMe()
        {
            D.Trace("[Spawner] DestroyMe");
            GetComponent<Collider2D>().enabled = false;
            GameObject go = gameController.PrefabFactory.CreatePrefab("EffectKickSmall", transform.position);
            transform.DOScale(6.0f, 0.5f);
            transform.DOLocalMoveX(transform.position.x + 100, 1.5f);
            transform.DOLocalMoveX(transform.position.y + 100, 1.5f);
            transform.GetComponent<SpriteRenderer>().DOFade(0.25f, 4.0f);
            Destroy(gameObject, 0.49f);
        }
    }
}