﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class Drop : Collectable
    {
        private string[] weapons = new string[] { "knife", "pistol", "rifle", "star" };
        public override void CollectMe()
        {
            base.CollectMe();
            int i = Random.Range(0, weapons.Length);
            gameController.PlayerController.ChangeWeapon(weapons[i]);
            Destroy(gameObject);
        }
    }
}