﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class Missle : Enemy
    {
        private void OnTriggerEnter2D(Collider2D collision)
        {
            D.Trace("[Missle] OnCollisionEnter2D");

            if (collision.gameObject.tag == "Player")
            {
                gameController.PrefabFactory.CreatePrefab("EffectKickBig", collision.gameObject.transform.position);
                gameController.RemoveLife();
            }
        }
        public override void EnemyDestroyed()
        {
            base.EnemyDestroyed();
            gameController.PrefabFactory.CreatePrefab("EffectScore100", transform.position);
            gameController.AddScore(100);
            gameController.AddPower(10);
        }
    }
}