﻿using Game.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Game
{
    public class Plane : Enemy
    {
        public override void EnemyDestroyed()
        {
            base.EnemyDestroyed();
            GameObject go = gameController.PrefabFactory.CreatePrefab("EffectKickSmall", transform.position);
            gameController.WinGame();
        }

        public override void Damage(int damageAmount)
        {
            GameObject go = gameController.PrefabFactory.CreatePrefab("EffectDamage1", transform.position);
            SpriteRenderer sr = transform.GetComponent<SpriteRenderer>();
            Sequence hitSequence = DOTween.Sequence();
            hitSequence
                .Append(sr.DOColor(Color.red, 0.25f))
                .Append(sr.DOColor(Color.white, 0.25f))
                .Append(sr.DOColor(Color.red, 0.25f))
                .Append(sr.DOColor(Color.white, 0.25f));

            hitSequence.Play();
            GameManager.Instance.Level.EnemyPower -= damageAmount;

            if (GameManager.Instance.Level.EnemyPower <= 0)
            {
                EnemyDestroyed();
            }
        }
    }
}