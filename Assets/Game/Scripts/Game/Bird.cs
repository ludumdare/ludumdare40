﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class Bird : Obstacle
    {
        private void OnTriggerEnter2D(Collider2D collision)
        {
            D.Trace("[Weapon] OnCollisionEnter2D");
            D.Trace("- collided with {0}", collision.transform.tag);

            //  if player push him back a few pixels

            if (collision.gameObject.tag == "Player")
            {
                collision.gameObject.transform.Translate(Vector2.left * 300.0f * Time.deltaTime);
            }
        }
        private void OnTriggerStay2D(Collider2D collision)
        {
            D.Trace("[Weapon] OnCollisionEnter2D");
            D.Trace("- collided with {0}", collision.transform.tag);

            //  if player push him back a few pixels

            if (collision.gameObject.tag == "Player")
            {
                collision.gameObject.transform.Translate(Vector2.left * 200.0f * Time.deltaTime);
            }
        }

        public override void DestroyMe()
        {
            base.DestroyMe();
            gameController.PrefabFactory.CreatePrefab("EffectScore1", transform.position);
            gameController.AddScore(1);
            gameController.AddPower(1);
        }
    }
}