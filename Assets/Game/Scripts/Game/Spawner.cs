﻿using Game.Controllers;
using Sdn.SpriteBoss;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    //  A spawner is an Object that will be
    //  created in Random spots at certain 
    //  intervals
    public class Spawner : Pauseable
    {
        public float Speed;

        protected SpriteBoss spriteBoss;

        public virtual void InitSpawner()
        {
            D.Trace("[Spawner] InitSpawner");
        }

        public virtual void StartSpawner()
        {
            D.Trace("[Spawner] StartSpawner");
        }

        public virtual void StopSpawner()
        {
            D.Trace("[Spawner] StopSpawner");
        }

        public virtual void UpdateSpawner()
        {
            if (IsPaused)
                return;

            transform.Translate(Vector2.left * Speed * Time.deltaTime);

            if (transform.position.x < -500)
            {
                Destroy(gameObject);
            }
        }

        private void Update()
        {
            UpdateSpawner();
        }

        public override void OnEnable()
        {
            base.OnEnable();
            spriteBoss = GetComponent<SpriteBoss>();
        }
    }
}