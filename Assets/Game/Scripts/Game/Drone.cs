﻿using Sdn.SpriteBoss;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class Drone : Enemy
    {
        public int ShootingCycle;
        public int ShootingPattern;
        public int ShootingSpeed;
        public GameObject[] ShootingObjects;

        private int cycle;

        public override void UpdateSpawner()
        {
            base.UpdateSpawner();

            cycle += 1;

            if (cycle > ShootingCycle)
            {
                cycle = 0;

                GameObject go = Instantiate(ShootingObjects[Random.Range(0, ShootingObjects.Length)], transform.position, Quaternion.identity);
                SpriteBoss sb = go.GetComponent<SpriteBoss>();
                sb.MoveInDirection(225, ShootingSpeed + Speed);
            }
        }

    }
}