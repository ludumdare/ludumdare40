﻿using UnityEngine;
using System.Collections;
using Game.Controllers;

namespace Game
{
    public class Pauseable : MonoBehaviour
    {
        protected GameController gameController;

        private bool paused;

        public bool IsPaused
        {
            get { return paused; }
        }

        public virtual void Pause()
        {
            paused = true;
        }

        public virtual void Resume()
        {
            paused = false;
        }

        public virtual void OnEnable()
        {
            gameController = GameObject.FindObjectOfType<GameController>();
        }
    }

   
}