﻿using Game.Controllers;
using Game.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class Kick : MonoBehaviour
    {
        private GameController gameController;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            D.Trace("[Kick] OnTriggerEnter2D");
            D.Detail("- collided with {0}", collision.transform.tag);

            if (collision.gameObject.tag == "Enemy")
            {
                SoundManager.Instance.PlaySound("punch");
                Enemy e = (Enemy)collision.gameObject.GetComponent<Enemy>();
                gameController.AddPower(e.PowerValue);
                gameController.PrefabFactory.CreatePrefab("EffectKickSmall", transform.position);
                e.EnemyDestroyed();
            }

            if (collision.gameObject.tag == "Plane")
            {
                SoundManager.Instance.PlaySound("punch");
                Enemy e = (Enemy)collision.gameObject.GetComponent<Enemy>();
                gameController.AddPower(e.PowerValue);
                gameController.PrefabFactory.CreatePrefab("EffectKickSmall", transform.position);
                e.Damage(1);
            }

            if (collision.gameObject.tag == "Obstacle")
            {
                Obstacle o = (Obstacle)collision.gameObject.GetComponent<Obstacle>();
                o.DestroyMe();
            }

            gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            gameController = GameObject.FindObjectOfType<GameController>();

        }
    }
}