﻿using Sdn.SpriteBoss;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class Weapon : Pauseable
    {
        public int ActiveTime;      //  time that weapon is available
        public int DelayTime;       //  time between firing
        public float Lifespan;      //  lifetime of weapon projectile
        public float Speed;         //  speed of projectile
        public float Rotation;      //  rotation speed (if any)
        public int Damage;
        public bool Auto;

        private SpriteBoss spriteBoss;
        private bool isMoving;

        public virtual void FireWeapon()
        {
            //  if we have lifespan, start that
            if (Lifespan > 0)
                Destroy(gameObject, Lifespan);

            isMoving = true;
        }

        private void Update()
        {
            if (IsPaused)
                return;

            if (!isMoving)
                return;

            transform.Translate(Vector2.right * Speed * Time.deltaTime);

        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            D.Trace("[Weapon] OnCollisionEnter2D");
            D.Trace("- collided with {0}", collision.transform.tag);

            if (collision.gameObject.tag == "Enemy")
            {
                Enemy e = (Enemy)collision.gameObject.GetComponent<Enemy>();
                e.Damage(Damage);
                gameController.AddPower(e.PowerValue);
                Destroy(gameObject);
            }

            if (collision.gameObject.tag == "Plane")
            {
                if (collision.gameObject != null)
                {
                    Enemy e = (Enemy)collision.gameObject.GetComponent<Enemy>();
                    e.Damage(Damage);
                    gameController.AddPower(e.PowerValue);
                    Destroy(gameObject);
                }
            }

            if (collision.gameObject.tag == "Obstacle")
            {
                Obstacle o = (Obstacle)collision.gameObject.GetComponent<Obstacle>();
                o.DestroyMe();
            }
        }

        public override void OnEnable()
        {
            base.OnEnable();
            spriteBoss = GetComponent<SpriteBoss>();
        }
    }
}