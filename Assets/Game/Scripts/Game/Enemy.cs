﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Game
{
    public class Enemy : Spawner
    {
        public int BaseDamage;
        public int PowerValue;

        public bool hit;

        public virtual void Damage(int damageAmount)
        {

            BaseDamage -= damageAmount;

            if (BaseDamage <= 0)
            {
                BaseDamage = 0;
                EnemyDestroyed();
            }
        }

        public virtual void EnemyDestroyed()
        {
            Destroy(gameObject);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (gameObject.tag == "Plane")
                return;

            D.Trace("[Weapon] OnCollisionEnter2D");
            D.Trace("- collided with {0}", collision.transform.tag);

            //  if player push him back a few pixels

            if (collision.gameObject.tag == "Player")
            {
                collision.gameObject.transform.Translate(Vector2.left * 300.0f * Time.deltaTime);
                if (!hit)
                    gameController.AddPower((-PowerValue/2));
                hit = true;
            }
        }
        private void OnTriggerStay2D(Collider2D collision)
        {
            D.Trace("[Weapon] OnCollisionEnter2D");
            D.Trace("- collided with {0}", collision.transform.tag);

            //  if player push him back a few pixels

            if (collision.gameObject.tag == "Player")
            {
                collision.gameObject.transform.Translate(Vector2.left * 200.0f * Time.deltaTime);
            }
        }
        public override void OnEnable()
        {
            base.OnEnable();
        }

    }
}