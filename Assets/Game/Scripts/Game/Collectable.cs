﻿using Game.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    /*
     *  An object that will be collected in the
     *  Game by the Player for Points or
     *  Advancement.
     *  
     */
    public class Collectable : Spawner
    {
        public int Value = 10;   //  added to score
        public float Gravity = 2;

        private int stage;
        private int lastStage;

        private int cycle;
        private int nextCycle;

        public override void OnEnable()
        {
            base.OnEnable();
            lastStage = 4;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Player")
            {
                CollectMe();
                Destroy(gameObject);
            }
        }

        public virtual void CollectMe()
        {
            D.Trace("[Collectible] CollectMe");
            gameController.AddScore(Value);
            string effectValue = "EffectScore" + Value.ToString();
            gameController.PrefabFactory.CreatePrefab(effectValue);
        }

        public override void UpdateSpawner()
        {
            if (IsPaused)
                return;

            if (stage > lastStage)
                return;

            cycle += 1;

            if (cycle > nextCycle)
            {
                stage += 1;
                cycle = 0;
            }

            if (stage == 1)
            {
                nextCycle = 100;
                transform.Translate(Vector2.up * (Speed*2) * Time.deltaTime);
                transform.Translate(Vector2.left * (Speed/(Gravity/2)) * Time.deltaTime);
            }

            if (stage == 2)
            {
                nextCycle = 50;
                transform.Translate(Vector2.left * (Speed / Gravity) * Time.deltaTime);
            }

            if (stage == 3)
            {
                nextCycle = 1000;
                transform.Translate(Vector2.down * (Speed / Gravity * 2) * Time.deltaTime);
                transform.Translate(Vector2.left * (Speed / Gravity) * Time.deltaTime);
            }

            if (stage == 4)
            {
                Destroy(gameObject);
            }

        }
    }
}