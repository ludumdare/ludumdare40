﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class Player : Pauseable
    {
        public float Speed;
        public float Gravity;
    }
}