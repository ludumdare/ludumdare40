﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Data
{
    [System.Serializable]
    public class PlayerData
    {
        public int Index;
        public int Level;
        public int Score;
        public int BestScore;
    }
}