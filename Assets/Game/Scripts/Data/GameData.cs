﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Data
{
    [CreateAssetMenu]
    [System.Serializable]
    public class GameData : ScriptableObject
    {
        [SerializeField]
        public string Title;
        [SerializeField]
        public string Version;
        [SerializeField]
        public string Copyright;
        [SerializeField]
        public string Message;
        [SerializeField]
        public int MaxSize;         //  what is the max zoom for the screen size (1, 2, 3, 4, etc)
        [SerializeField]
        public string HomeScene;
        [SerializeField]
        public string AboutScene;
        [SerializeField]
        public string InfoScene;
        [SerializeField]
        public string SettingsScene;
        [SerializeField]
        public string GameScene;
        [SerializeField]
        public LevelData Level;
    }
}