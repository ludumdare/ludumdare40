﻿using Game.Controllers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Data
{
    [CreateAssetMenu]
    [System.Serializable]
    public class SpawnerData: ScriptableObject
    {
        public SpawnController Spawner;
        public int MinCycle;
        public int MaxCycle;
        public int MinSpeed;
        public int MaxSpeed;
    }
}