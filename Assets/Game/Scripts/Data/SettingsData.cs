﻿using UnityEngine;

namespace Game.Data
{
    [CreateAssetMenu]
    [System.Serializable]
    public class SettingsData : ScriptableObject
    {
        [SerializeField]
        public KeyCode ControlLeft;
        [SerializeField]
        public KeyCode ControlRight;
        [SerializeField]
        public KeyCode ControlUp;
        [SerializeField]
        public KeyCode ControlDown;
        [SerializeField]
        public KeyCode ControlAction;
        [SerializeField]
        public bool FullScreen;     //  is the game fullscreen?
        [SerializeField]
        public int ScreenSize;      //  what is the size of the game screen (1x, 2x, 3x)?
        [SerializeField]
        public int LastSave;        //  last save position (1-3)
        [SerializeField]
        public bool CheatsOn;       //  cheat codes on
    }
}
