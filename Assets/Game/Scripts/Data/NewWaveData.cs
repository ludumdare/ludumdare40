﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Data
{
    [CreateAssetMenu]
    [System.Serializable]
    public class NewWaveData : ScriptableObject
    {
        public string Name;
        public List<SpawnerData> Spawners;
        public int WaveCycles;
    }
}