﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Data
{
    [CreateAssetMenu]
    [System.Serializable]
    public class SessionData
    {
        public int Level;
        public int Stage;
        public int Wave;
        public int Lives;
        public int Score;
        public int BestScore;
        public int Power;
        public int PowerNeeded;
    }
}