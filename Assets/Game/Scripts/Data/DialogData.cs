﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Data
{
    [CreateAssetMenu]
    [System.Serializable]
    public class DialogData : ScriptableObject
    {
        public string DialogText;
        public string DialogActor;
        public string DialogDelay;
    }
}