﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Data
{
    [CreateAssetMenu]
    [System.Serializable]
    public class LevelData :ScriptableObject
    {
        public string Name;
        public string EnemyName;
        public int EnemyPower;
        public int EnemyStartingPower;
        public List<StageData> LevelStages;
    }
}