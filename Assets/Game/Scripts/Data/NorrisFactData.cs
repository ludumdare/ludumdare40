﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Data
{
    [CreateAssetMenu]
    [System.Serializable]
    public class NorrisFactData : ScriptableObject
    {
        public string NorrisFact;
    }
}