﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Data
{
    [CreateAssetMenu]
    [System.Serializable]
    public class StageData : ScriptableObject
    {
        public string StageName;
        public List<WaveData> StageWaves;
    }
}