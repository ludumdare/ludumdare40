﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Data
{
    [CreateAssetMenu]
    [System.Serializable]
    public class ScriptData : ScriptableObject
    {
        List<DialogData> ScriptDialog;
        public ScriptActions NextScriptAction;
        public string NextActionName;
    }

    public enum ScriptActions
    {
        ACTION_NONE,
        ACTION_DIALOG,
        ACTION_PLAY,

    }
}