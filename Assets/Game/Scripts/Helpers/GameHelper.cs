﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Helpers
{

    public static class GameHelper
    {
        public static void ReloadCurrentScene()
        {
            D.Trace("[GameManager] ReloadCurrentScene");
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}