﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

using Game.Helpers;
using Game.Managers;

namespace Game.Helpers
{
    public class ScaleHelper : MonoBehaviour
    {
        public int BaseWidth;
        public int BaseHeight;
        public Canvas[] ScaledCanvas;
        public Text ScaledText;

        public void NextScale()
        {
            D.Trace("[ScaleHelper] NextScale");
            GameManager.Instance.Settings.ScreenSize += 1;
            if (GameManager.Instance.Settings.ScreenSize > GameManager.Instance.Game.MaxSize)
                GameManager.Instance.Settings.ScreenSize = 1;
            scaleGame();
        }

        private void Start()
        {
            D.Trace("[ScaleHelper] NextScale");
            scaleGame();
            scaleCanvas();
        }

        private void Update()
        {
            D.Fine("[ScaleHelper] Update");
            if (Application.isEditor)
                return;
            scaleCanvas();
        }

        private void scaleCanvas()
        {
            D.Trace("[ScaleHelper] scaleCanvas");
            foreach (Canvas c in ScaledCanvas)
            {
                CanvasScaler cs = c.GetComponent<CanvasScaler>();
                cs.uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;
                cs.scaleFactor = (GameManager.Instance.Settings.ScreenSize);
                cs.referencePixelsPerUnit = 100;
            }
        }

        private void scaleGame()
        {
            D.Trace("[ScaleHelper] scaleGame");
            int scale = GameManager.Instance.Settings.ScreenSize;
            bool fullScreen = false;

            if (scale == 1)
            {
                ScaledText.text = "2X";
            }
            if (scale == 2)
            {
                ScaledText.text = "3X";
            }
            if (scale == 3)
            {
                ScaledText.text = "FULLSCREEN";
            }
            if (scale == 4)
            {
                ScaledText.text = "1X";
                fullScreen = true;
            }
            Screen.SetResolution(BaseWidth * scale, BaseHeight * scale, fullScreen);
        }

    }
}