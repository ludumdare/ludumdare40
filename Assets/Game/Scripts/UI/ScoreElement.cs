﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    public class ScoreElement : Element
    {
        public Text scoreText;

        private int score;

        protected override void UpdateElement()
        {
            if (!isActive)
                return;

            score = Mathf.CeilToInt(Mathf.Lerp((float)score, (float)sessionData.Score, Time.deltaTime));
            scoreText.text = string.Format("{0:000000}", score);
            
        }
    }
}