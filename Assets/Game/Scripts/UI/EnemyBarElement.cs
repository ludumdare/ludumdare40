﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    public class EnemyBarElement : BarElement
    {
        public override void InitElement()
        {
            base.InitElement();
            MaxValue = levelData.EnemyStartingPower;
        }
        protected override void UpdateElement()
        {
            Value = levelData.EnemyPower;
            base.UpdateElement();
        }
    }
}