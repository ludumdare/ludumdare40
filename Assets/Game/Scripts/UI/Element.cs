﻿using Game.Controllers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Game.Managers;
using Game.Data;

namespace Game.UI
{
    
    public class Element : MonoBehaviour
    {
        protected GameController gameController;
        protected GameManager gameManager;
        protected SessionData sessionData;
        protected LevelData levelData;

        protected bool isActive;

        public virtual void InitElement()
        {
            gameController = GameObject.FindObjectOfType<GameController>();
            gameManager = GameManager.Instance;
            sessionData = gameManager.Session;
            levelData = gameManager.Level;
        }

        public virtual void StartElement()
        {
            isActive = true;
        }

        public virtual void StopElement()
        {
            isActive = false;
        }
        protected virtual void UpdateElement()
        {
        }

        private void Update()
        {
            if (!isActive)
                return;

            UpdateElement();    
        }
    }
}