﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    public class PowerBarElement : BarElement
    {
        public override void InitElement()
        {
            base.InitElement();
            MaxValue = sessionData.PowerNeeded;
        }

        protected override void UpdateElement()
        {
            base.UpdateElement();
            Value = sessionData.Power;
        }
    }
}