﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    public class BarElement : Element
    {
        public RectTransform barRect;
        public int MaxValue;
        public int BarSize;
        public int Value;

        protected override void UpdateElement()
        {
            if (!isActive)
                return;

            if (Value > 0)
            {
                float pct = (float)Value / (float)MaxValue;
                float size = BarSize * pct;
                barRect.sizeDelta = new Vector2(size, 15);
            }
        }
    }
}