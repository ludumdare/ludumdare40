﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Game.Effects
{
    public class Effect : Pauseable
    {
        public override void OnEnable()
        {
            base.OnEnable();
            transform.GetComponent<SpriteRenderer>().DOFade(0.0f, 1.0f);
            Destroy(gameObject, 0.7f);
        }
    }
}