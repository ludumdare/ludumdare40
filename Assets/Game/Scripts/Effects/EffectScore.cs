﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Game.Effects
{
    public class EffectScore : Effect
    {
        public override void OnEnable()
        {
            transform.DOMoveY(transform.position.y + 175, 2.0f);
            transform.DOMoveX(transform.position.x + Random.Range(-10, 10), 2.0f);
            transform.GetComponent<SpriteRenderer>().DOFade(0.5f, 2.0f);
            Destroy(gameObject, 1.9f);
        }
    }
}