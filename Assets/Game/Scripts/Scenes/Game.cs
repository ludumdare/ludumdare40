﻿using Game.Controllers;
using Game.Data;
using Game.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Scenes
{
    public class Game : MonoBehaviour
    {
        public GameData DefaultGameData;
        public SettingsData DefaultSettingsData;

        private GameController gameController;
        private GameManager gameManager;
        private Camera gameCamera;

        private void Start()
        {
            gameManager.StartNewGame();
            gameController.StartGame();
        }

        private void OnEnable()
        {
            D.Trace("[Game] OnEnable");

            gameController = GameObject.FindObjectOfType<GameController>();
            gameManager = GameManager.Instance;
            gameCamera = Camera.main;

            //gameManager.Game = DefaultGameData;
            //gameManager.Settings = DefaultSettingsData;
            //gameManager.Level = DefaultGameData.Level;

            //gameManager.Settings = gameManager.LoadGameSettings(gameManager.Settings);

            //if (gameManager.Settings.LastSave > 0)
            //{
            //    gameManager.Player = gameManager.LoadPlayer(gameManager.Settings.LastSave);
            //}
        }
    }
}