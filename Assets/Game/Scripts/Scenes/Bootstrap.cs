﻿using Game.Data;
using Game.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Scenes
{
    public class Bootstrap : MonoBehaviour
    {
        public GameData DefaultGameData;
        public SettingsData DefaultSettingsData;

        private GameManager gameManager;

        void Start()
        {
            SceneManager.LoadScene(GameManager.Instance.Game.AboutScene);
        }
        private void OnEnable()
        {
            D.Trace("[Game] OnEnable");

            gameManager = GameManager.Instance;


            gameManager.Game = DefaultGameData;
            gameManager.Settings = DefaultSettingsData;
            gameManager.Level = DefaultGameData.Level;

            gameManager.Settings = gameManager.LoadGameSettings(gameManager.Settings);

            if (gameManager.Settings.LastSave > 0)
            {
                gameManager.Player = gameManager.LoadPlayer(gameManager.Settings.LastSave);
            }
        }
    }
}