﻿using Game.Data;
using Game.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game.Scenes
{
    public class Home : MonoBehaviour
    {
        public GameData DefaultGameData;
        public SettingsData DefaultSettingsData;
        public Text versionText;

        private GameManager gameManager;


        public void ButtonStart()
        {
            SceneManager.LoadScene("travel");
        }

        public void ButtonStats()
        {

        }

        public void ButtonAbout()
        {

        }

        private void OnEnable()
        {

            gameManager = GameManager.Instance;

            gameManager.Game = DefaultGameData;
            gameManager.Settings = DefaultSettingsData;

            gameManager.Settings = gameManager.LoadGameSettings(gameManager.Settings);

            if (gameManager.Settings.LastSave > 0)
            {
                gameManager.Player = gameManager.LoadPlayer(gameManager.Settings.LastSave);
            }

            versionText.text = string.Format("v{0}", gameManager.Game.Version);
        }
    }
}