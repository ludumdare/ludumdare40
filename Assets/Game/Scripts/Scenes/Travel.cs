﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.UI;

namespace Game.Scenes
{
    public class Travel : MonoBehaviour
    {
        public SpriteRenderer spriteFader;
        public Text textFader;

        private void Start()
        {
            Sequence seq = DOTween.Sequence();
            seq.Append(spriteFader.DOFade(1.0f, 1.0f));
            seq.Append(spriteFader.DOFade(0.0f, 2.0f));
            seq.Append(spriteFader.DOFade(0.0f, 3.0f));
            seq.Append(spriteFader.DOFade(1.0f, 2.0f));
            seq.AppendCallback(nextScene);

            Sequence seq1 = DOTween.Sequence();
            seq1.Append(textFader.DOFade(0.0f, 2.0f));
            seq1.Append(textFader.DOFade(1.0f, 1.0f));

            seq.Play();
            seq1.Play();
        }

        private void Update()
        {
            if (Input.GetButton("Fire1"))
            {
                nextScene();
            }
        }
        
        private void nextScene()
        {
            SceneManager.LoadScene("Work");
        }
    }
}